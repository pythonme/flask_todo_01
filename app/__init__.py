from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# Sqlite Database absolute path
file_path = os.path.abspath(os.getcwd())+"/todo.db"

app = Flask(__name__)

# Connect to Sqlite DB
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+file_path
db = SQLAlchemy(app)

from app import routes
