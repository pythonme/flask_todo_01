# flask_todo_01


## Getting started

### Create sqlite db

```bash
cd flask_todo_01
sudo apt-get install sqlite
sqlite3 todo.db
```

```sql
CREATE TABLE todo (
	id INTEGER PRIMARY KEY,
	text TEXT NOT NULL,
	complete INTEGER NOT NULL
);
```

### Install libs and run
```bash
cd flask_todo_01

# First time install python and pip packages via pipenv
pipenv shell
pipenv install


# Run
python main.py
```


